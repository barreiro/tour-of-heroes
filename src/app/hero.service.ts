import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};

@Injectable()
export class HeroService {

  private heroesUrl = 'api/heroes';

  constructor(
  	private http: HttpClient,
  	private messageService: MessageService
  ) { }

  private log(message: string) {
  	this.messageService.add(`HeroService: ${message}`);
  }

  private handleError<T> (operation = 'operation', result?: T) {
  	return (error: any): Observable<T> => {
  		console.log(error);

  		this.log(`${operation} falló: ${error.message}`);

  		return of(result as T);
  	}
  }

  getHeroes(): Observable<Hero[]> {
  	
  	return this.http.get<Hero[]>(this.heroesUrl)
  		.pipe(
  				catchError(this.handleError('getHeroes', [])),
  				tap(heroes => {if (heroes.length>0) this.log("Heroes obtenidos") } )
  			);;
  }

  getHero(id: number): Observable<Hero> {
  	const url = `${this.heroesUrl}/${id}`;

  	return this.http.get<Hero>(url)
  		.pipe(
  				tap( _ => this.log(`Obtenido héroe con id = ${id}`)),
  				catchError(this.handleError<Hero>('getHero'))
  			);
  }

  updateHero (hero: Hero): Observable<any> {
    return this.http.put(this.heroesUrl, hero, httpOptions)
      .pipe(
          tap( _ => this.log(`Actualizado héroe con id ${hero.id}`)),
          catchError(this.handleError<any>('updateHero'))
        );
  }

  addHero (hero: Hero): Observable<Hero> {
    return this.http.post<Hero>(this.heroesUrl, hero, httpOptions)
      .pipe(
          tap( (hero: Hero) => this.log(`Añadido héroe con id ${hero.id}`) ),
          catchError(this.handleError<Hero>('addHero'))
        );
  }

  deleteHero (hero: Hero | number): Observable<Hero> {
    const id = typeof hero === 'number' ? hero : hero.id;
    const url = `${this.heroesUrl}/${id}`;

    return this.http.delete<Hero>(url, httpOptions)
      .pipe(
        tap( _ => this.log(`Eliminado héroe con id ${id}`) ),
          catchError(this.handleError<Hero>('deleteHero'))
        );

  }

  searchHero(term: string): Observable<Hero[]> {
    if (!term.trim()) {
      return of([]);
    }

    return this.http.get<Hero[]>(`api/heroes/?name=${term}`)
      .pipe(
        tap( _ => this.log(`Encontrados heroes que coinciden con ${term}`) ),
          catchError(this.handleError<Hero[]>('searchHero', []))
        );
  }

}
